<?php

namespace app\components;

use Yii;
use yii\base\ActionFilter;

class AdminFilter extends ActionFilter
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest)
            return Yii::$app->getResponse()->redirect(['/user/security/login']);
        else
            return !Yii::$app->user->isGuest && Yii::$app->user->identity->username=='admin';
    }
}