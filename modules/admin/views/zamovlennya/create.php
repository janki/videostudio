<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Zamovlennya */

$this->title = 'Створити замовлення';
$this->params['breadcrumbs'][] = ['label' => 'Замовлення', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zamovlennya-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
