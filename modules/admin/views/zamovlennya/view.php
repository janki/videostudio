<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Zamovlennya */

$this->title = '#' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Замовлення', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zamovlennya-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user.profile.name',
            'fio',
            'phone',
            'comment',
            'status',
            'link',
            'robota.type',
            'vuconavec.pib:text:Виконавець',
            'plan_cina',
            'date_start',
            'date_end',
            'fact_cina',
        ],
    ]) ?>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви впевнені, що хоче видалити це замовлення?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
