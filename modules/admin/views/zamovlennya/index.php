<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ZamovlennyaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Замовлення';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zamovlennya-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'status',
//            'user.profile.name',
            'fio:text:Клієнт',
            'phone',
            'robota.type',
            'vuconavec.pib:text:Виконавець',
            // 'plan_cina',
             'date_start',
             'date_end',
             'fact_cina',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
