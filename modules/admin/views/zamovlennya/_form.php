<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use app\models\Robota;
use app\models\Vuconavec;
use app\models\VuconavecRobota;

/* @var $this yii\web\View */
/* @var $model app\models\Zamovlennya */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zamovlennya-form row">

    <div class="col-md-6">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'user.profile.name',
                'fio',
                'phone',
                'link',
                'robota.type',
                'vuconavec.pib:text:Виконавець',
                'plan_cina',
                'date_start',
                'date_end',
                'fact_cina',
                'comment',
            ],
        ]) ?>

    </div>

    <div class="col-md-6">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'status')->dropDownList(['new' => 'new', 'working' => 'working', 'done' => 'done']); ?>

        <?php $robotas = Robota::find()->all(); ?>

        <?= $form->field($model, 'robota_id')->dropDownList(ArrayHelper::map($robotas, 'id', 'type'),
            array('onChange' => 'showVuconavec(this.options[this.selectedIndex].value);')); ?>

        <?php foreach($robotas as $robota) { ?>
            <div class="vuconavecs-of" id="vuconavec_of_<?= $robota->id ?>" style="display: none;">
                <?= $form->field($model, 'vuconavec_id')
                    ->dropDownList(ArrayHelper::map(VuconavecRobota::find()->where(['robota_id' => $robota->id])->all(), 'vuconavec.id', 'vuconavec.pib'),
                        array('class' => 'vuconavec-list form-control', 'id' => 'vuconavec_list_'.$robota->id)); ?>
            </div>
        <?php } ?>

        <?= $form->field($model, 'plan_cina')->textInput() ?>

        <?= $form->field($model, 'date_start')->textInput() ?>

        <?= $form->field($model, 'date_end')->textInput() ?>

        <?= $form->field($model, 'fact_cina')->textInput() ?>

        <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Зберегти',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function showVuconavec(robota_id) {
        var vuconavecs = document.getElementsByClassName('vuconavecs-of'), i;
        for (var i = 0; i < vuconavecs.length; i ++) {
            vuconavecs[i].style.display = 'none';
        }
        vuconavecs = document.getElementsByClassName('vuconavec-list'), i;
        for (var i = 0; i < vuconavecs.length; i ++) {
            vuconavecs[i].name = 'blank'
        }

        var e = document.getElementById("vuconavec_of_" + robota_id);
        e.style.display = 'block';
        e = document.getElementById("vuconavec_list_" + robota_id)
        e.name = 'Zamovlennya[vuconavec_id]';
    }
    <?php if (isset($model->robota)) { ?>
    showVuconavec(<?= $model->robota->id ?>);
    <?php } elseif (count($robotas)>0) { ?>
    showVuconavec(<?= $robotas[0]->id ?>);
    <?php } ?>
</script>
