<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Robota */

$this->title = 'Тип робіт #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Типи робіт', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="robota-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'plan_cina',
            'type',
            'link_rob',
            'timing',
        ],
    ]) ?>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви впевнені, що хочете видалити тип робіт?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
