<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Robota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="robota-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_cina')->textInput() ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_rob')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'timing')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Зберегти',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
