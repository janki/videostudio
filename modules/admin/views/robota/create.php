<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Robota */

$this->title = 'Create Robota';
$this->params['breadcrumbs'][] = ['label' => 'Robotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="robota-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
