<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Vuconavec */

$this->title = 'Створити';
$this->params['breadcrumbs'][] = ['label' => 'Виконавці', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vuconavec-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'robotas' => $robotas,
    ]) ?>

</div>
