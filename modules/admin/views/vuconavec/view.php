<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vuconavec */

$this->title = 'Виконавець #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Виконавці', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vuconavec-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pib',
            'posada',
        ],
    ]) ?>

    <div class="tags">
        Robotas:
        <ul>
            <?php foreach($model->getVuconavecRobotas()->all() as $robota) : ?>
                <li><?= $robota->getRobota()->one()->type ?></li>
            <?php endforeach; ?>
        </ul>
    </div>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
