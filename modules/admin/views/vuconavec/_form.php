<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Vuconavec */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vuconavec-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pib')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'posada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'robotas')->listBox(
        ArrayHelper::map($robotas, 'id', 'type'),
        [
            'multiple' => true
        ]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Редагувати', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
