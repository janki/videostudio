<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VuconavecSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Виконавці';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vuconavec-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Створити', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'pib',
            'posada',
            'zamovlennya_cnt',
            'zamovlennya_sum',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
