<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vuconavec */

$this->title = 'Редагування: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Виконавці', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="vuconavec-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'robotas' => $robotas,
    ]) ?>

</div>
