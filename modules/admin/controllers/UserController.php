<?php
namespace app\modules\admin\controllers;

use dektrium\user\controllers\AdminController as BaseAdminController;

class UserController extends BaseAdminController
{
    public $layout = '/admin';
}