<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Zamovlennya;
use app\models\ZamovlennyaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ZamovlennyaController implements the CRUD actions for Zamovlennya model.
 */
class ZamovlennyaController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],// add all actions to take guest to login page
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zamovlennya models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZamovlennyaSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zamovlennya model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Zamovlennya model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//        var_dump(Yii::$app->user->identity->profile->phone);
//        die();
        $model = new Zamovlennya();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            $model->status = 'new';

            $model->plan_cina = $model->robota->plan_cina;
            if ($model->save()) {
//                Yii::app()->user->setFlash('success', "Дякуємо за замовлення, з Вами зв'яжуться в найближчий час!");
                \Yii::$app->getSession()->setFlash('success', "Дякуємо за замовлення, з Вами зв'яжуться в найближчий час!");
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->user_id = Yii::$app->user->id;
            $model->status = 'new';
            $model->fio = Yii::$app->user->identity->profile->name;
            $model->phone = Yii::$app->user->identity->profile->phone;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Zamovlennya model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->plan_cina = $model->robota->plan_cina;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', "Замовлення успішно змінено!");
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Zamovlennya model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zamovlennya model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zamovlennya the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zamovlennya::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
