<?php

namespace app\controllers;

use Yii;
use app\models\ZamovlennyaSearch;

class PortfolioController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new ZamovlennyaSearch();
        $searchModel->status = 'done';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView()
    {
        return $this->render('view');
    }
}
