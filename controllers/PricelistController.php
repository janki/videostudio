<?php

namespace app\controllers;

use Yii;
use app\models\RobotaSearch;

class PricelistController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new RobotaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
