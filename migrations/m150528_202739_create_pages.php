<?php

use yii\db\Schema;
use yii\db\Migration;

class m150528_202739_create_pages extends Migration
{
    public function up()
    {
        $this->createTable('pages', array(
            'id' => 'pk',
            'title' => 'VARCHAR(255) NOT NULL',
            'body' => 'TEXT',
        ));

    }

    public function down()
    {
        $this->dropTable('pages');
    }
}
