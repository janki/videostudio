<?php

use yii\db\Schema;
use yii\db\Migration;

class m150516_122830_create_robota extends Migration
{
    public function up()
    {
        $this->createTable('robota', array(
            'id' => 'pk',
            'plan_cina' => 'FLOAT NOT NULL',
            'type' => 'VARCHAR(50)',
            'link_rob' => 'VARCHAR(50)',
            'timing' => 'FLOAT NOT NULL',
        ));
    }

    public function down()
    {
        $this->dropTable('robota');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
