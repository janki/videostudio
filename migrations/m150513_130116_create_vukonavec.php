<?php

use yii\db\Schema;
use yii\db\Migration;

class m150513_130116_create_vukonavec extends Migration
{
    public function up()
    {
        $this->createTable('vuconavec', array(
            'id' => 'pk',
            'pib' => 'VARCHAR(50) NOT NULL',
            'posada' => 'VARCHAR(50)',
        ));
    }

    public function down()
    {
        $this->dropTable('vuconavec');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
