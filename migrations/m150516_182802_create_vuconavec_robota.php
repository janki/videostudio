<?php

use yii\db\Schema;
use yii\db\Migration;

class m150516_182802_create_vuconavec_robota extends Migration
{
    public function up()
    {
        $this->createTable('vuconavec_robota', array(
            'id' => 'pk',
            'vuconavec_id' => 'INT(11) NOT NULL',
            'robota_id' => 'INT(11) NOT NULL',
        ));
        $this->addForeignKey('vuconavec_id', 'vuconavec_robota', 'vuconavec_id', 'vuconavec', 'id', 'RESTRICT');
        $this->addForeignKey('robota_id', 'vuconavec_robota', 'robota_id', 'robota', 'id', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('vuconavec_robota');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
