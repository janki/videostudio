<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_180620_add_fields_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'phone', 'VARCHAR(255)');
    }

    public function down()
    {
        $this->dropColumn('profile', 'phone');
    }
}
