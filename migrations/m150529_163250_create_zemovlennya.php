<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_163250_create_zemovlennya extends Migration
{
    public function up()
    {
        $this->createTable('zamovlennya', array(
            'id' => 'pk',
            'user_id' => 'INT(11) NOT NULL',
            'fio' => 'VARCHAR(255) NOT NULL',
            'phone' => 'INT(20) NOT NULL',
            'comment' => 'VARCHAR(500)',
            'status' => 'varchar(10)',
            'link' => 'varchar(255)',
            'robota_id' => 'INT(11) NOT NULL',
            'vuconavec_id' => 'INT(11) NOT NULL',
            'plan_cina' => 'FLOAT',
            'date_start' => 'DATE',
            'date_end' => 'DATE',
            'fact_cina' => 'FLOAT',
        ));
//        $this->addForeignKey('user_id', 'zamovlennya', 'user_id', 'user', 'id', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('zamovlennya');
    }
}
