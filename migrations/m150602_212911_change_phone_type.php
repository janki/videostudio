<?php

use yii\db\Schema;
use yii\db\Migration;

class m150602_212911_change_phone_type extends Migration
{
    public function up()
    {
        $this->alterColumn('zamovlennya', 'phone', 'VARCHAR(255)');
    }

    public function down()
    {
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
