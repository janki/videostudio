<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Page;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'VideoStudio',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
//            $pages = Page::find()->all();
//            $links = array();
//            foreach($pages as $page) {
//                $links[] = ['label' => $page->title, 'url' => ['/page/view', 'id' => $page->id]];
//            }
//            echo Nav::widget([
//                'options' => ['class' => 'navbar-nav navbar-left'],
//                'items' => $links,
//            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items' => [
                    ['label' => 'Контакти', 'url' => ['/page/view', 'id' => 1]],
                    ['label' => 'Про комапнію', 'url' => ['/page/view', 'id' => 2]],
                    ['label' => 'Прайслист', 'url' => ['/pricelist/index']],
                    ['label' => 'Портфоліо', 'url' => ['/portfolio/index']],
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => Yii::$app->user->isGuest ? [
                        ['label' => 'Реєстрація', 'url' => ['/user/registration/register']],
                        ['label' => 'Вхід', 'url' => ['/user/security/login']]
                ] : [
                        ['label' => 'Мої Замовлення', 'url' => ['/zamovlennya/index']],
                        ['label' => 'Адмін Панель', 'url' => ['/admin/default/index'],
                            'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->username=='admin' ],
                        ['label' => 'Вийти (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?php Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; VideoStudio <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
