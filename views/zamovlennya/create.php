<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Zamovlennya */

$this->title = 'Створити замовлення';
$this->params['breadcrumbs'][] = ['label' => 'Замовлення', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('/user/settings/_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">

<div class="zamovlennya-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

            </div>
        </div>
    </div>
</div>