<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Zamovlennya */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\ArrayHelper;
use app\models\Robota;
use app\models\Vuconavec;
use app\models\VuconavecRobota;

?>

<div class="zamovlennya-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput() ?>

    <?= $form->field($model, 'phone') ?>

    <?php $robotas = Robota::find()->all(); ?>

    <?= $form->field($model, 'robota_id')->dropDownList(ArrayHelper::map($robotas, 'id', 'type'),
        array('onChange' => 'showVuconavec(this.options[this.selectedIndex].value);')); ?>

    <?php foreach($robotas as $robota) { ?>
        <div class="vuconavecs-of" id="vuconavec_of_<?= $robota->id ?>" style="display: none;">
        <?= $form->field($model, 'vuconavec_id')
            ->dropDownList(ArrayHelper::map(VuconavecRobota::find()->where(['robota_id' => $robota->id])->all(), 'vuconavec.id', 'vuconavec.pib'),
                array('class' => 'vuconavec-list form-control', 'id' => 'vuconavec_list_'.$robota->id)); ?>
        </div>
    <?php } ?>

    <?= $form->field($model, 'comment')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Зберегти',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function showVuconavec(robota_id) {
        var vuconavecs = document.getElementsByClassName('vuconavecs-of'), i;
        for (var i = 0; i < vuconavecs.length; i ++) {
            vuconavecs[i].style.display = 'none';
        }
        vuconavecs = document.getElementsByClassName('vuconavec-list'), i;
        for (var i = 0; i < vuconavecs.length; i ++) {
            vuconavecs[i].name = 'blank'
        }

        var e = document.getElementById("vuconavec_of_" + robota_id);
        e.style.display = 'block';
        e = document.getElementById("vuconavec_list_" + robota_id)
        e.name = 'Zamovlennya[vuconavec_id]';
    }
    <?php if (count($robotas)>0) { ?>
    showVuconavec(<?= $robotas[0]->id ?>);
    <?php } ?>
</script>
