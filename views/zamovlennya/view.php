<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Zamovlennya */

$this->title = 'Замовлення #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Zamovlennyas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('/user/settings/_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">

<div class="zamovlennya-view">

    <?php if ($message = Yii::$app->session->getFlash('success')) { ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fio:text:Клієнт',
            'phone',
            'status',
            'robota.type',
            'vuconavec.pib:text:Виконавець',
            'plan_cina',
            'date_start',
            'date_end',
            'fact_cina',
            'link:url',
            'comment',
        ],
    ]) ?>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви впевнені, що хочете видалити замовлення?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>

            </div>
        </div>
    </div>
</div>