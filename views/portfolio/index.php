<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ZamovlennyaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Портфоліо';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'robota.type',
            'vuconavec.pib:text:Виконавець',
            'date_start',
            'date_end',
            'fact_cina',
            'link:url',
        ],
    ]); ?>

</div>
