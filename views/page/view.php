<?php
/* @var $this yii\web\View */
/* @var $model app\models\Page */
?>
<h1><?= $model->title ?></h1>

<p>
    <?= $model->body ?>
</p>
