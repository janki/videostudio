<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vuconavec;

/**
 * VuconavecSearch represents the model behind the search form about `app\models\Vuconavec`.
 */
class VuconavecSearch extends Vuconavec
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['pib', 'posada'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vuconavec::find();
        $query
            ->select(['vuconavec.*, count(zamovlennya.id) as zamovlennya_cnt,
                       sum(zamovlennya.fact_cina) as zamovlennya_sum'])
            ->joinWith('zamovlennyas')
            ->groupBy('vuconavec.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['zamovlennya_cnt'] = [
            'asc' => ['zamovlennya_cnt' => SORT_ASC],
            'desc' => ['zamovlennya_cnt' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['zamovlennya_sum'] = [
            'asc' => ['zamovlennya_sum' => SORT_ASC],
            'desc' => ['zamovlennya_sum' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'pib', $this->pib])
            ->andFilterWhere(['like', 'posada', $this->posada]);

        return $dataProvider;
    }
}
