<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vuconavec_robota".
 *
 * @property integer $id
 * @property integer $vuconavec_id
 * @property integer $robota_id
 *
 * @property Robota $robota
 * @property Vuconavec $vuconavec
 */
class VuconavecRobota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vuconavec_robota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vuconavec_id', 'robota_id'], 'required'],
            [['vuconavec_id', 'robota_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vuconavec_id' => 'Vuconavec ID',
            'robota_id' => 'Robota ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRobota()
    {
        return $this->hasOne(Robota::className(), ['id' => 'robota_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVuconavec()
    {
        return $this->hasOne(Vuconavec::className(), ['id' => 'vuconavec_id']);
    }
}
