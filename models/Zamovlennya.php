<?php

namespace app\models;

use Yii;

use dektrium\user\models\User;

/**
 * This is the model class for table "zamovlennya".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $status
 * @property string $link
 * @property integer $robota_id
 * @property integer $vuconavec_id
 * @property double $plan_cina
 * @property string $date_start
 * @property string $date_end
 * @property double $fact_cina
 *
 * @property User $user
 */
class Zamovlennya extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zamovlennya';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'robota_id', 'vuconavec_id', 'fio', 'phone'], 'required'],
            [['user_id', 'robota_id', 'vuconavec_id'], 'integer'],
            [['plan_cina', 'fact_cina'], 'number'],
            [['date_start', 'date_end', 'comment'], 'safe'],
            [['status'], 'string', 'max' => 10],
            [['link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Статус',
            'link' => 'Посилання',
            'fio' => 'П.І.Б.',
            'phone' => 'Номер телефону',
            'comment' => 'Коментар до замовлення',
            'robota_id' => 'Тип роботи',
            'vuconavec_id' => 'Виконавець',
            'plan_cina' => 'Планова ціна',
            'date_start' => 'Дата початку',
            'date_end' => 'Дата закінчення',
            'fact_cina' => 'Фактична ціна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRobota() {
        return $this->hasOne(Robota::className(), ['id' => 'robota_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVuconavec() {
        return $this->hasOne(Vuconavec::className(), ['id' => 'vuconavec_id']);
    }
}
