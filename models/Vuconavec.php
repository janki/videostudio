<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "vuconavec".
 *
 * @property integer $id
 * @property string $pib
 * @property string $posada
 *
 * @property VuconavecRobota[] $vuconavecRobotas
 */
class Vuconavec extends \yii\db\ActiveRecord
{
    /**
     * Список работ, закреплённых исполнителем.
     * @var array
     */
    protected $robotas = [];

    /**
     * Кількість виконаних замовлень
     * @var integer
     */
    public $zamovlennya_cnt = 0;

    /**
     * Вартість виконаних замовлень
     * @var integer
     */
    public $zamovlennya_sum = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vuconavec';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pib'], 'required'],
            [['pib', 'posada'], 'string', 'max' => 50],
            [['robotas'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pib' => 'П.І.Б.',
            'posada' => 'Посада',
            'robotas' => 'Типи робіт',
            'zamovlennya_cnt' => 'Кількість замовлень',
            'zamovlennya_sum' => 'Загальна винагорода'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        VuconavecRobota::deleteAll(['vuconavec_id' => $this->id]);
        $values = [];
        foreach ($this->robotas as $id) {
            $values[] = [$this->id, $id];
        }
        self::getDb()->createCommand()
            ->batchInsert(VuconavecRobota::tableName(), ['vuconavec_id', 'robota_id'], $values)->execute();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVuconavecRobotas()
    {
        return $this->hasMany(VuconavecRobota::className(), ['vuconavec_id' => 'id']);
    }

    /**
     * Устанавлиает работы исполнителя.
     * @param $robotasId
     */
    public function setRobotas($robotasId)
    {
        $this->robotas = (array) $robotasId;
    }

    /**
     * Возвращает массив идентификаторов работ.
     */
    public function getRobotas()
    {
        return ArrayHelper::getColumn(
            $this->getVuconavecRobotas()->all(), 'robota_id'
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZamovlennyas()
    {
        return $this->hasMany(Zamovlennya::className(), ['vuconavec_id' => 'id']);
    }
}
