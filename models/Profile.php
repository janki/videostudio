<?php

namespace app\models;

use dektrium\user\models\Profile as BaseProfile;

class Profile extends BaseProfile
{
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = 'phone';
        $scenarios['update'][]   = 'phone';
//        $scenarios['register'][] = 'field';
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
//        $rules['fieldRequired'] = ['field', 'required'];
        $rules['fieldLength'] = ['phone', 'string', 'max' => 25];

        return $rules;
    }
}