<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "robota".
 *
 * @property integer $id
 * @property double $plan_cina
 * @property string $type
 * @property string $link_rob
 * @property double $timing
 *
 * @property VuconavecRobota[] $vuconavecRobotas
 */
class Robota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'robota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_cina', 'timing'], 'required'],
            [['plan_cina', 'timing'], 'number'],
            [['type', 'link_rob'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_cina' => 'Планова ціна',
            'type' => 'Назва типу роботи',
            'link_rob' => 'Посилання',
            'timing' => 'Тривалість',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVuconavecRobotas()
    {
        return $this->hasMany(VuconavecRobota::className(), ['robota_id' => 'id']);
    }
}
