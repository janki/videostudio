<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Zamovlennya;

/**
 * ZamovlennyaSearch represents the model behind the search form about `app\models\Zamovlennya`.
 */
class ZamovlennyaSearch extends Zamovlennya
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'robota_id', 'vuconavec_id'], 'integer'],
            [['fio', 'phone', 'status', 'link', 'date_start', 'date_end'], 'safe'],
            [['plan_cina', 'fact_cina'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zamovlennya::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'robota_id' => $this->robota_id,
            'vuconavec_id' => $this->vuconavec_id,
            'plan_cina' => $this->plan_cina,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'fact_cina' => $this->fact_cina,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
